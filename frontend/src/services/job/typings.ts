// @ts-ignore
/* eslint-disable */
declare namespace API {
  type Job = {

    jobId: number;

    /**
     * 任务名称
     */

    jobName: string;

    /**
     * 任务组名
     */

    jobGroup: string;

    /**
     * 调用目标字符串
     */

    invokeTarget: string;

    /**
     * cron执行表达式
     */

    cronExpression: string

    params: { [key: string]: any };
    /**
     * cron计划策略
     */

    misfirePolicy: string;

    /**
     * 是否并发执行（0允许 1禁止）
     */
    concurrent: string | '0' | '1';

    /**
     * 任务状态（0正常 1暂停）
     */
    status: string | '0' | '1';
    createTime: string;
    updateTime: string;
    createBy: string;
    updateBy: string;
    nextValidTime: string;
  };
  type TableDataInfo<T>={
    /** 总记录数 */
    total:number;

    /** 列表数据 */
     rows:T[];

    /** 消息状态码 */
     code:number;

    /** 消息内容 */
    msg:string;
  }
  type Result<T> ={
   code:number;
   msg:string;
   data:T;
  }
  // type PageParams = {
  //   current?: number;
  //   pageSize?: number;
  // };
}
