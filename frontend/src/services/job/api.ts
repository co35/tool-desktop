// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

/** 获取任务列表 GET /job/list */
export async function listJobs( params:{
  /** 当前的页码 */
  pageNum?: number;
  /** 页面的容量 */
  pageSize?: number;
},options?: { [key: string]: any }) {
  return request<
    API.TableDataInfo<API.Job>
  >('/api/job/list', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/**
 * 根据任务ID获取任务详情 GET /job/{jobId}
 */
export async function getJob(jobId:number,options?: { [key: string]: any }) {
  return request<API.Result<API.Job>>(`/api/job`,{
    method: 'GET',
    params: {
      jobId:jobId,
    },
    ...(options || {}),
  } )
}

/**
 * 新增任务 POST /job
 */
export async function addJob(params:API.Job,options?: { [key: string]: any }) {
  return request<API.Result<API.Job>>('/api/job',{
    method: 'POST',
    data: params,
    ...(options || {}),
  })
}

/**
 * 修改任务 PUT /job
 */
export async function updateJob(data:API.Job,options?: { [key: string]: any }) {
  return request<
    API.Result<any>
  >('/api/job', {
    method: 'PUT',
    data,
    ...(options || {}),
  });
}

/**
 * 删除任务 DELETE /job
 * @param jobId 任务id
 */
export async function delJob(jobIds:number[]){
  return request<API.Result<any>>(`/api/job`,{
    method:'DELETE',
    params: {
      jobIds:jobIds,
    }
  })
}
