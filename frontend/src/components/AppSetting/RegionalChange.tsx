import type { Settings as LayoutSettings } from '@ant-design/pro-components';
import { useIntl } from '@umijs/max';
import { List, Switch } from 'antd';
import React from 'react';
import { renderLayoutSettingItem } from './LayoutChange';

const RegionalChange: React.FC<{
  settings: Partial<LayoutSettings> | undefined;
  changeSetting: (key: string, value: any, hideLoading?: boolean) => void;
}> = ({ settings = {}, changeSetting }) => {
  const formatMessage = useIntl().formatMessage;
  const regionalSetting = ['header', 'footer', 'menu', 'menuHeader'];
  return (
    <List
      style={{ width: '100%' }}
      split={false}
      renderItem={renderLayoutSettingItem}
      dataSource={regionalSetting.map((key) => {
        return {
          title: formatMessage({ id: `app.setting.regionalsettings.${key}` }),
          action: (
            <Switch
              size="small"
              className={`regional-${key}`}
              checked={settings[`${key}Render`] || settings[`${key}Render`] === undefined}
              onChange={(checked) =>
                changeSetting(`${key}Render`, checked === true ? undefined : false)
              }
            />
          ),
        };
      })}
    />
  );
};

export default RegionalChange;
