import { CheckOutlined } from '@ant-design/icons';
import { useIntl } from '@umijs/max';
import { Tooltip } from 'antd';
import React from 'react';
import styles from './ThemeColor.less';

interface TagProps {
  color: string;
  check: boolean;
  title: string;
  onChange?: (value: string) => void;
}

interface ThemeColorProps {
  colorList: { key: string; color: string }[];
  value: string | undefined;
  onClick?: (color: string) => void;
}

const Tag: React.FC<TagProps> = (props) => {
  const { color, check, title, onChange } = props;
  const intl = useIntl();

  return (
    <Tooltip
      key={color}
      title={intl.formatMessage({
        id: `app.setting.themecolor.${title}`,
      })}
    >
      <div
        className={styles.tag}
        style={{ backgroundColor: color }}
        onClick={() => onChange && onChange(color)}
      >
        {check ? <CheckOutlined /> : ''}
      </div>
    </Tooltip>
  );
};

const ThemeColor: React.FC<ThemeColorProps> = (props) => {
  const { colorList, value, onClick } = props;

  return (
    // @ts-ignore
    <div>
      {colorList?.map(({ key, color }) => {
        return (
          <Tag key={key} title={key} check={value === color} color={color} onChange={onClick} />
        );
      })}
    </div>
  );
};

export default ThemeColor;
