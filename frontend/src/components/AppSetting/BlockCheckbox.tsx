import { CheckOutlined } from '@ant-design/icons';
import { Tooltip } from 'antd';
import classNames from 'classnames';
import React from 'react';

export type BlockCheckboxProps = {
  value?: string;
  onChange: (key: string) => void;
  list?: {
    title: string;
    key: string;
  }[];
  prefixCls: string;
};

const BlockCheckbox: React.FC<BlockCheckboxProps> = ({ value, onChange, list, prefixCls }) => {
  const baseClassName = prefixCls;

  return (
    <div
      className={baseClassName}
      style={{
        minHeight: 42,
      }}
    >
      {list?.map((item) => (
        <Tooltip title={item.title} key={item.key}>
          <div
            className={classNames(`${baseClassName}-${item.key}`)}
            onClick={() => onChange(item.key)}
          >
            <CheckOutlined
              className={`${baseClassName}-selectIcon`}
              style={{
                display: value === item.key ? 'block' : 'none',
              }}
            />
          </div>
        </Tooltip>
      ))}
    </div>
  );
};

export default BlockCheckbox;
