import type { MenuTheme } from 'antd';
import { Switch } from 'antd';
import React from 'react';
import styles from './SwitchTheme.less';

interface SwitchThemeProps {
  theme: MenuTheme | 'realDark' | undefined;
  onClick?: (checked: boolean) => void;
}

const Sun = () => <div>🌞</div>;
const Moon = () => <div>🌜</div>;

/**
 * 切换浅色模式或黑暗模式
 * @constructor
 */
const SwitchTheme: React.FC<SwitchThemeProps> = (props) => {
  const { theme, onClick } = props;

  return (
    <Switch
      {...props}
      className={styles.switch}
      checkedChildren={<Sun />}
      unCheckedChildren={<Moon />}
      checked={theme === 'light'}
      onClick={onClick}
    />
  );
};

export default SwitchTheme;
