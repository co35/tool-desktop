import BlockCheckbox from '@/components/AppSetting/BlockCheckbox';
import LayoutChange, { renderLayoutSettingItem } from '@/components/AppSetting/LayoutChange';
import RegionalChange from '@/components/AppSetting/RegionalChange';
import SwitchTheme from '@/components/AppSetting/SwitchTheme';
import ThemeColor from '@/components/AppSetting/ThemeColor';
import { getLanguage } from '@/locales';
import { useModel } from '@@/exports';
import type { ProSettings } from '@ant-design/pro-components';
import { isBrowser } from '@ant-design/pro-components';
import { useIntl } from '@umijs/max';
import {
  disable as darkreaderDisable,
  enable as darkreaderEnable,
  setFetchMethod as setFetch,
} from '@umijs/ssr-darkreader';
import { useUrlSearchParams } from '@umijs/use-params';
import { Button, ConfigProvider, Divider, Drawer, List, message, Row, Switch } from 'antd';
import { merge, omit } from 'lodash';
import useMergedState from 'rc-util/lib/hooks/useMergedState';
import React, { useEffect, useRef, useState } from 'react';
import defaultSettings from '../../../config/defaultSettings';
import './index.less';

type MergerSettingsType<T> = Partial<T> & {
  primaryColor?: string;
  colorWeak?: boolean;
};
export type SettingDrawerProps = {
  defaultSettings?: MergerSettingsType<ProSettings>;
  settings?: MergerSettingsType<ProSettings>;
  collapse?: boolean;
  onCollapseChange?: (collapse: boolean) => void;
  getContainer?: any;
  hideHintAlert?: boolean;
  hideCopyButton?: boolean;
  /** 使用实验性质的黑色主题 */
  enableDarkTheme?: boolean;
  prefixCls?: string;
  colorList?: { key: string; color: string }[];
  onSettingChange?: (settings: MergerSettingsType<ProSettings>) => void;
  pathname?: string;
  disableUrlParams?: boolean;
  themeOnly?: boolean;
};
const themeConfig = {
  daybreak: '#1890ff',
  dust: '#F5222D',
  volcano: '#FA541C',
  sunset: '#FAAD14',
  cyan: '#13C2C2',
  green: '#52C41A',
  geekblue: '#2F54EB',
  purple: '#722ED1',
};

/**
 * Daybreak-> #1890ff
 *
 * @param val
 */
export function genStringToTheme(val?: string): string {
  return val && themeConfig[val] ? themeConfig[val] : val;
}

const getDifferentSetting = (state: Partial<ProSettings>): Record<string, any> => {
  const stateObj: Partial<ProSettings> = {};
  Object.keys(state).forEach((key) => {
    if (state[key] !== defaultSettings[key] && key !== 'collapse') {
      stateObj[key] = state[key];
    } else {
      stateObj[key] = undefined;
    }
    if (key.includes('Render')) stateObj[key] = state[key] === false ? false : undefined;
  });
  stateObj.menu = undefined;
  return stateObj;
};

const genCopySettingJson = (settingState: MergerSettingsType<ProSettings>) =>
  JSON.stringify(
    omit(
      {
        ...settingState,
        primaryColor: settingState.primaryColor,
      },
      ['colorWeak'],
    ),
    null,
    2,
  );
const updateTheme = async (dark: boolean, color?: string) => {
  if (typeof window === 'undefined') return;
  if (typeof window.MutationObserver === 'undefined') return;

  if (!ConfigProvider.config) return;
  ConfigProvider.config({
    theme: {
      primaryColor: genStringToTheme(color) || '#1890ff',
    },
  });

  if (dark) {
    const defaultTheme = {
      brightness: 100,
      contrast: 90,
      sepia: 10,
    };

    const defaultFixes = {
      invert: [],
      css: '',
      ignoreInlineStyle: ['.react-switch-handle'],
      ignoreImageAnalysis: [],
      disableStyleSheetsProxy: true,
    };
    if (window.MutationObserver && window.fetch) {
      setFetch(window.fetch);
      darkreaderEnable(defaultTheme, defaultFixes);
    }
  } else {
    if (window.MutationObserver) darkreaderDisable();
  }
};

/**
 * 初始化的时候需要做的工作
 *
 * @param urlParams
 * @param settings
 * @param onSettingChange
 */
const initState = (
  urlParams: Record<string, any>,
  settings: Partial<ProSettings>,
  onSettingChange: SettingDrawerProps['onSettingChange'],
) => {
  if (!isBrowser()) return;

  const replaceSetting = {};
  Object.keys(urlParams).forEach((key) => {
    if (defaultSettings[key] || defaultSettings[key] === undefined) {
      if (key === 'primaryColor') {
        replaceSetting[key] = genStringToTheme(urlParams[key]);
        return;
      }
      replaceSetting[key] = urlParams[key];
    }
  });
  const newSettings: MergerSettingsType<ProSettings> = merge({}, settings, replaceSetting);
  delete newSettings.menu;
  delete newSettings.title;
  delete newSettings.iconfontUrl;

  // 同步数据到外部
  onSettingChange?.(newSettings);

  // 如果 url 中设置主题，进行一次加载。
  if (defaultSettings.navTheme !== urlParams.navTheme && urlParams.navTheme) {
    updateTheme(settings.navTheme === 'realDark', urlParams.primaryColor);
  }
};

const getParamsFromUrl = (
  urlParams: Record<string, any>,
  settings?: MergerSettingsType<ProSettings>,
) => {
  if (!isBrowser()) return defaultSettings;

  return {
    ...defaultSettings,
    ...(settings || {}),
    ...urlParams,
  };
};

const AppSetting: React.FC<SettingDrawerProps> = (props) => {
  const {
    defaultSettings: propsDefaultSettings = undefined,
    settings: propsSettings = undefined,
    colorList = [
      { key: 'daybreak', color: '#1890ff' },
      { key: 'dust', color: '#F5222D' },
      { key: 'volcano', color: '#FA541C' },
      { key: 'sunset', color: '#FAAD14' },
      { key: 'cyan', color: '#13C2C2' },
      { key: 'green', color: '#52C41A' },
      { key: 'geekblue', color: '#2F54EB' },
      { key: 'purple', color: '#722ED1' },
    ],
    onSettingChange,
    prefixCls = 'block',
    pathname = window.location.pathname,
    disableUrlParams = true,
  } = props;
  const firstRender = useRef<boolean>(true);
  const { visible, close } = useModel('system');
  const [language, setLanguage] = useState<string>(getLanguage());
  const [urlParams, setUrlParams] = useUrlSearchParams(
    {},
    {
      disabled: disableUrlParams,
    },
  );

  const [settingState, setSettingState] = useMergedState<Partial<ProSettings>>(
    () => getParamsFromUrl(urlParams, propsSettings || propsDefaultSettings),
    {
      value: propsSettings,
      onChange: onSettingChange,
    },
  );

  useEffect(() => {
    // 语言修改，这个是和 locale 是配置起来的
    const onLanguageChange = (): void => {
      if (language !== getLanguage()) {
        setLanguage(getLanguage());
      }
    };

    /** 如果不是浏览器 都没有必要做了 */
    if (!isBrowser()) return () => null;
    initState(getParamsFromUrl(urlParams, propsSettings), settingState, setSettingState);
    window.document.addEventListener('languagechange', onLanguageChange, {
      passive: true,
    });

    return () => window.document.removeEventListener('languagechange', onLanguageChange);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useEffect(() => {
    updateTheme(settingState.navTheme === 'realDark', settingState.primaryColor);
  }, [settingState.primaryColor, settingState.navTheme]);
  useEffect(() => {
    /** 如果不是浏览器 都没有必要做了 */
    if (!isBrowser()) return;
    if (disableUrlParams) return;
    if (firstRender.current) {
      firstRender.current = false;
      return;
    }

    /** 每次从url拿最新的防止记忆 */
    const urlSearchParams = new URLSearchParams(window.location.search);
    const params = Object.fromEntries(urlSearchParams.entries());
    const diffParams = getDifferentSetting({ ...params, ...settingState });

    delete diffParams.logo;
    delete diffParams.menu;
    delete diffParams.title;
    delete diffParams.iconfontUrl;
    delete diffParams.pwa;

    setUrlParams(diffParams);
  }, [setUrlParams, settingState, urlParams, pathname, disableUrlParams]);

  const intl = useIntl();
  const { initialState, setInitialState, refresh } = useModel('@@initialState');

  const baseClassName = `${prefixCls}-checkbox`;
  /**
   * 修改设置
   *
   * @param key
   * @param value
   */
  const changeSetting = (key: string, value: string | boolean) => {
    const nextState = {} as any;
    nextState[key] = value;

    if (key === 'layout') {
      nextState.contentWidth = value === 'top' ? 'Fixed' : 'Fluid';
    }
    if (key === 'layout' && value !== 'mix') {
      nextState.splitMenus = false;
    }
    if (key === 'layout' && value === 'mix') {
      nextState.navTheme = 'light';
    }
    if (key === 'colorWeak' && value === true) {
      const dom = document.querySelector('body');
      if (dom) {
        dom.dataset.prosettingdrawer = dom.style.filter;
        dom.style.filter = 'invert(80%)';
      }
    }
    if (key === 'colorWeak' && value === false) {
      const dom = document.querySelector('body');
      if (dom) {
        dom.style.filter = dom.dataset.prosettingdrawer || 'none';
        delete dom.dataset.prosettingdrawer;
      }
    }
    delete nextState.menu;
    delete nextState.title;
    delete nextState.iconfontUrl;
    delete nextState.logo;
    delete nextState.pwa;

    const nextSettings = { ...initialState?.settings, ...nextState };
    setInitialState((preInitialState) => ({
      ...preInitialState,
      settings: nextSettings,
    }));
  };

  const switchTheme = (checked: boolean) => {
    const theme = checked ? 'light' : 'realDark';
    changeSetting('navTheme', theme);
  };

  const switchColor = (color: string) => {
    changeSetting('primaryColor', color);
  };
  const switchLayout = (value: string) => {
    changeSetting('layout', value);
  };
  // 导航列表
  const boxList = [
    {
      key: 'side',
      title: intl.formatMessage({ id: 'app.setting.sidemenu' }),
    },
    {
      key: 'top',
      title: intl.formatMessage({ id: 'app.setting.topmenu' }),
    },
    {
      key: 'mix',
      title: intl.formatMessage({ id: 'app.setting.mixmenu' }),
    },
  ];

  // @ts-ignore
  return (
    <Drawer
      width={300}
      closable={false}
      title={intl.formatMessage({
        id: 'app.setting.title',
        defaultMessage: 'Page style setting',
      })}
      placement="right"
      onClose={close}
      visible={visible}
    >
      <Divider>
        {intl.formatMessage({
          id: 'app.setting.pagestyle',
          defaultMessage: 'Page style setting',
        })}
      </Divider>
      <Row justify={'center'}>
        <SwitchTheme theme={initialState?.settings?.navTheme} onClick={switchTheme} />
      </Row>
      <Divider>
        {intl.formatMessage({
          id: 'app.setting.themecolor',
          defaultMessage: 'Theme color',
        })}
      </Divider>
      <Row justify={'center'}>
        <ThemeColor
          colorList={colorList}
          value={initialState?.settings?.primaryColor}
          onClick={switchColor}
        />
      </Row>
      <Divider>
        {intl.formatMessage({
          id: 'app.setting.navigationmode',
          defaultMessage: 'Navigation Mode',
        })}
      </Divider>
      <Row justify={'center'}>
        <BlockCheckbox
          prefixCls={baseClassName}
          value={initialState?.settings?.layout}
          list={boxList}
          onChange={switchLayout}
        />
      </Row>
      <Row justify={'center'}>
        <LayoutChange settings={initialState?.settings} changeSetting={changeSetting} />
      </Row>
      <Divider>
        {intl.formatMessage({
          id: 'app.setting.regionalsettings',
          defaultMessage: 'Regional Settings',
        })}
      </Divider>
      <Row justify={'center'}>
        <RegionalChange settings={initialState?.settings} changeSetting={changeSetting} />
      </Row>
      <Divider>
        {intl.formatMessage({
          id: 'app.setting.othersettings',
          defaultMessage: 'Other Settings',
        })}
      </Divider>
      <Row justify={'center'}>
        <List
          style={{ width: '100%' }}
          split={false}
          renderItem={renderLayoutSettingItem}
          dataSource={[
            {
              title: intl.formatMessage({ id: 'app.setting.weakmode' }),
              action: (
                <Switch
                  size="small"
                  className="color-weak"
                  checked={initialState?.settings?.colorWeak}
                  onChange={(checked) => {
                    changeSetting('colorWeak', checked);
                  }}
                />
              ),
            },
          ]}
        />
      </Row>
      <Divider />
      <Row gutter={[16, 16]} justify={'center'}>
        <Button
          block
          onClick={async () => {
            try {
              await navigator.clipboard.writeText(genCopySettingJson(settingState));
              message.success(intl.formatMessage({ id: 'app.setting.copyinfo' }));
            } catch (error) {
              // console.log(error);
            }
          }}
        >
          应用
        </Button>
        <Button block danger onClick={refresh}>
          重置
        </Button>
      </Row>
    </Drawer>
  );
};

export default AppSetting;
