//node 后端路由
export const ipcApiRoute = {
  test: 'controller.example.test',
  messageShow: 'controller.example.messageShow',
  messageShowConfirm: 'controller.example.messageShowConfirm',
  selectFolder: 'controller.example.selectFolder',
  openDirectory: 'controller.example.openDirectory',
  loadViewContent: 'controller.example.loadViewContent',
  removeViewContent: 'controller.example.removeViewContent',
  createWindow: 'controller.example.createWindow',
  sendNotification: 'controller.example.sendNotification',
  initPowerMonitor: 'controller.example.initPowerMonitor',
  getScreen: 'controller.example.getScreen',
  openSoftware: 'controller.example.openSoftware',
  autoLaunch: 'controller.example.autoLaunch',
  setTheme: 'controller.example.setTheme',
  getTheme: 'controller.example.getTheme',
  checkForUpdater: 'controller.example.checkForUpdater',
  downloadApp: 'controller.example.downloadApp',
  dbOperation: 'controller.example.dbOperation',
  uploadFile: 'controller.example.uploadFile',
  checkHttpServer: 'controller.example.checkHttpServer',
  doHttpRequest: 'controller.example.doHttpRequest',
  doSocketRequest: 'controller.example.doSocketRequest',
  ipcInvokeMsg: 'controller.example.ipcInvokeMsg',
  ipcSendSyncMsg: 'controller.example.ipcSendSyncMsg',
  ipcSendMsg: 'controller.example.ipcSendMsg',
  hello: 'controller.example.hello',
};

export const specialIpcRoute = {
  appUpdater: 'app.updater', // 此频道在后端也有相同定义
};
