const { ipcRenderer: ipc } = (window.require && window.require('electron')) || {};
// @ts-ignore
/**
 * （将废弃，请使用 $ipcInvoke 代替）异步调用主函数
 // * @param ipc
 * @param channel
 * @param param
 * @returns {Promise<unknown>}
 */
// const call = ( channel: any, param: any): Promise<any> => {
//   return new Promise((resolve) => {
//     ipcRenderer.ipc.once(channel, (event, result) => {
//       console.log('[ipcRenderer] [call] result:', result)
//       resolve(result)
//     })
//     ipcRenderer.ipc.send(channel, param)
//   })
// }

/**
 * 发送异步消息（invoke/handle 模型）
 * @param channel
 * @param param
 * @returns {Promise}
 */
const invoke = (channel: string, param: any): Promise<any> => {
  const message = ipc.invoke(channel, param);
  return message;
};

/**
 * 发送同步消息（send/on 模型）
 * @param channel
 * @param param
 * @returns {Any}
 */
const sendSync = (channel: string, param: any) => {
  const message = ipc.sendSync(channel, param);
  return message;
};

export default {
  ipc,
  // call,
  invoke,
  sendSync,
};
