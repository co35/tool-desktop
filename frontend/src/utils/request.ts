/** 配置request请求时的默认参数 */
import type { RequestConfig } from "@@/plugin-request/request";

// 重写请求

const authHeaderInterceptor = (url: string, options: RequestConfig) => {

  console.log('request', url, options);
  // const authHeader = { Authorization: 'Bearer xxxxxx' };
  return {
    url: `http://localhost:7423${url}`,
    // options: { ...options, interceptors: true, headers: authHeader },
    options: { ...options, interceptors: true},
  };
};




export default authHeaderInterceptor;
