import {
  AlignLeftOutlined,
  CaretRightOutlined,
  DeleteOutlined,
  DoubleRightOutlined,
  EditOutlined,
  EllipsisOutlined, EyeOutlined,
  PlusOutlined,
  VerticalAlignBottomOutlined
} from "@ant-design/icons";
import type {ActionType, ProColumns, ProFormInstance} from '@ant-design/pro-components';
import {PageContainer, ProDescriptions} from '@ant-design/pro-components';
import {ProFormRadio} from '@ant-design/pro-components';
import {ProFormSelect} from '@ant-design/pro-components';
import {ProFormText} from "@ant-design/pro-components";
import {ModalForm, ProForm, ProTable} from "@ant-design/pro-components";
import {Button, Dropdown, Menu, message, Modal, Popconfirm, Space, Tag} from "antd";
import React, {useRef, useState} from 'react';
import {addJob, delJob, getJob, listJobs, updateJob} from "@/services/job/api";
type EditFormProp = {
  opt: string;
  data: API.Job;
};
const GroupName = {
  'DEFAULT': '默认',
  'SYSTEM': '系统',
}
const options = {
  'edit': <Button size="small" type="link" icon={<EditOutlined/>}>
    修改
  </Button>,
};

const TableHeadMenu = (
  <Menu
    onClick={({key}) => {
      alert(key)
    }}
    items={[
      {
        key: '1',
        icon: <EditOutlined/>,
        label: '修改',
      },
      {
        key: '2',
        icon: <DeleteOutlined/>,
        label: '删除',
      },
      {
        key: '3',
        label: '导出',
        icon: <VerticalAlignBottomOutlined/>,
      },
      {
        key: '4',
        label: '日志',
        icon: <AlignLeftOutlined/>,
      }
    ]}
  />
);

const ButtonForm: React.FC<EditFormProp> = (props) => {
  const restFormRef = useRef<ProFormInstance>();
  const {opt, data} = props;
  const tooltipStr = 'Bean调用示例：sampleTask.params(\'das\')\n' +
    'Class类调用示例：com.dingdangmaoup.data.acquisition.system.tasker.task.params(\'das\')\n' +
    '参数说明：支持字符串，布尔类型，长整型，浮点型，整型'
  const edit = (
    <ModalForm<API.Job>
      formRef={restFormRef}
      onFinish={async (values) => {
        updateJob(values).then(res => {
          if (res.code === 200) {
            message.success('提交成功');
          }
        }).catch(err => {
          message.error(err.message);
          return false;
        })
        return true;
      }}
      submitTimeout={2000}
      title='修改任务'
      trigger={options[opt]}
      submitter={{
        searchConfig: {
          resetText: '重置',
        },
        resetButtonProps: {
          onClick: () => {
            restFormRef.current?.resetFields();
          },
        },
      }}
    >
      <ProForm.Group>
        <ProFormText
          hidden
          width="lg"
          name="jobId"
          label="任务编号"
          disabled={true}
          initialValue={data.jobId}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormText
          width="md"
          name="jobName"
          label="任务名称"
          initialValue={data.jobName}

        />
        <ProFormSelect
          width="md"
          options={[
            {
              value: 'DEFAULT',
              label: '默认',
            },
            {
              value: 'SYSTEM',
              label: '系统',
            }
          ]}
          name="jobGroup"
          label="任务分组"
          initialValue={data.jobGroup}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormText
          width="lg"
          name="invokeTarget"
          label="调用方法"
          tooltip={tooltipStr}
          initialValue={data.invokeTarget}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormText
          width="lg"
          name="cronExpression"
          label="cron表达式"
          initialValue={data.cronExpression}
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormRadio.Group
          width="lg"
          name="misfirePolicy"
          label="执行策略"
          radioType="button"
          initialValue={data.misfirePolicy}
          options={
            [
              {
                label: '立即执行',
                value: '1',
              },
              {
                label: '执行一次',
                value: '2',
              },
              {
                label: '放弃执行',
                value: '3',
              },
            ]
          }
        />
      </ProForm.Group>
      <ProForm.Group>
        <ProFormRadio.Group
          width="md"
          name="concurrent"
          label="是否并发"
          radioType="button"
          initialValue={data.concurrent}
          options={
            [
              {
                label: '允许',
                value: '0',
              },
              {
                label: '禁止',
                value: '1',
              },
            ]
          }
        />
        <ProFormRadio.Group
          width="md"
          name="status"
          label="状态"
          radioType="radio"
          initialValue={data.status}
          options={
            [
              {
                label: '正常',
                value: '0',
              },
              {
                label: '暂停',
                value: '1',
              },
            ]
          }
        />
      </ProForm.Group>
    </ModalForm>
  )
  const view = (<ModalForm<API.Job>
    formRef={restFormRef}
    onFinish={async (values) => {
      updateJob(values).then(res => {
        if (res.code === 200) {
          message.success('提交成功');
        }
      }).catch(err => {
        message.error(err.message);
        return false;
      })
      return true;
    }}
    submitTimeout={2000}
    title='任务详情'
    trigger={options[opt]}
    submitter={{
      searchConfig: {
        resetText: '重置',
      },
      resetButtonProps: {
        onClick: () => {
          restFormRef.current?.resetFields();
          //   setModalVisible(false);
        },
      },
    }}
  >
    <ProDescriptions title={"任务详情"}>a</ProDescriptions>
  </ModalForm>)
  if (opt === 'edit') {
    return edit;
  } else if (opt === 'view') {
    return view;
  } else {
    return <>a</>
  }
};
const renderRemoveUser = (text: string, data: API.Job) => (
  <Popconfirm key="popconfirm" title={`确认${text}吗?`} okText="是" cancelText="否" onConfirm={() => {
    const jobs = [data.jobId];
    delJob(jobs).then(r => {
      if (r.code == 200) {
        message.success('删除成功')
      }
    })
  }}>
    <Button size="small" key='del' type="link" icon={<DeleteOutlined/>}>
      {text}
    </Button>
  </Popconfirm>
);


export default () => {
  const [visible, setVisible] = useState<boolean>(false);
  const [addView, setAddView] = useState<boolean>(false);
  const [jobId, setJobId] = useState<number>(0);
  const actionRef = useRef<ActionType>();
  const OptionMenu = (data: API.Job) => (
    <Menu
      onClick={({key}) => {
        switch (key) {
          case "1":
            //立即执行
            Modal.confirm({
              title: `确认要立即执行一次"${data.jobName}"任务吗？`,
            })
            break;
          case "2":
            setJobId(data.jobId);
            setVisible(true);
        }
      }}
      items={[
        {
          key: '1',
          icon: <CaretRightOutlined/>,
          label: '执行一次',
        },
        {
          key: '2',
          label: '任务详情',
          icon: <EyeOutlined/>,
        },
        {
          key: '3',
          label: '调度日志',
          icon: <AlignLeftOutlined/>,
        }
      ]}
    />
  );
  const columns: ProColumns<API.Job>[] = [
    {
      title: '任务编号',
      dataIndex: 'jobId',
      fixed: 'left',
      align: 'center',
      ellipsis: true,
      width: 120,
    },
    {
      title: '任务名称',
      width: 120,
      dataIndex: 'jobName',
      align: 'center',
      ellipsis: true,
    },
    {
      title: '任务组名',
      width: 120,
      align: 'center',
      dataIndex: 'jobGroup',
      render: (text, row) => {
        return <Tag color="blue">{GroupName[row.jobGroup] ? GroupName[row.jobGroup] : row.jobGroup}</Tag>;
      }
    },
    {
      title: '调用目标字符串',
      width: 120,
      align: 'center',
      ellipsis: true,
      dataIndex: 'invokeTarget',
    },
    {
      title: 'cron执行表达式',
      width: 120,
      align: 'center',
      dataIndex: 'cronExpression',
    },
    {
      title: '任务状态',
      width: 120,
      align: 'center',
      dataIndex: 'status',
      valueType: 'select',
      valueEnum: {
        0: {text: '正常', status: 'Processing'},
        1: {text: '暂停', status: 'Error'},
      }
    },
    {
      title: '操作',
      width: 280,
      valueType: 'option',
      fixed: 'right',
      align: 'center',
      key: 'jobId',
      render: (text, record) => [
        <ButtonForm opt={'edit'} data={record} key='edit'/>,
        renderRemoveUser('删除', record),
        <Dropdown key="more" overlay={OptionMenu(record)}>
          <a>
            <Space>
              <DoubleRightOutlined/>
              更多
            </Space>
          </a>
        </Dropdown>,
      ],
    },
  ];

  return (
    <PageContainer>
      <ProTable<API.Job>
        columns={columns}
        actionRef={actionRef}
        cardBordered
        request={async (params, sort, filter) => {
          console.log(params)
          console.log(sort, filter);
          const query = {
            pageNum: params.current,
            ...params
          }
          delete query.current;
          const res = await listJobs(query);
          return {
            data: res.rows,
            success: res.code === 200,
            total: res.total,
          };
        }}
        columnsState={{
          persistenceKey: 'job_detail',
          persistenceType: 'localStorage',
          onChange(value) {
            console.log('value: ', value);
          },
        }}
        scroll={{x: 1000}}
        rowKey="jobId"
        rowSelection={{}}
        search={{
          labelWidth: 'auto',
        }}
        form={{
          // 由于配置了 transform，提交的参与与定义的不同这里需要转化一下
          syncToUrl: (values, type) => {
            if (type === 'get') {
              return {
                ...values,
                created_at: [values.startTime, values.endTime],
              };
            }
            return values;
          },
        }}
        pagination={{
          pageSize: 10,
          showSizeChanger: true,
          onChange: (page) => console.log(page),
        }}
        dateFormatter="string"
        headerTitle="任务列表"
        toolBarRender={() => [
          <Button key="button" icon={<PlusOutlined/>} type="primary" onClick={()=>(setAddView(true))}>
            新增
          </Button>,
          <Dropdown key="menu" overlay={TableHeadMenu}>
            <Button>
              <EllipsisOutlined/>
            </Button>
          </Dropdown>,
        ]}
      />
      <ModalForm<API.Job>
        visible={addView}
        onVisibleChange={setAddView}
        onFinish={async (values) => {
          addJob(values).then(res => {
            if (res.code === 200) {
              message.success('提交成功');
            }
          }).catch(err => {
            message.error(err.message);
            return false;
          })
          return true;
        }}
        submitTimeout={2000}
        title='新增任务'
        // trigger={options[opt]}
        // submitter={{
        //   searchConfig: {
        //     resetText: '重置',
        //   },
        //   resetButtonProps: {
        //     onClick: () => {
        //       restFormRef.current?.resetFields();
        //     },
        //   },
        // }}
      >
        <ProForm.Group>
          <ProFormText
            hidden
            width="lg"
            name="jobId"
            label="任务编号"
            disabled={true}
          />
        </ProForm.Group>
        <ProForm.Group>
          <ProFormText
            width="md"
            name="jobName"
            label="任务名称"

          />
          <ProFormSelect
            width="md"
            options={[
              {
                value: 'DEFAULT',
                label: '默认',
              },
              {
                value: 'SYSTEM',
                label: '系统',
              }
            ]}
            initialValue={'DEFAULT'}
            name="jobGroup"
            label="任务分组"
          />
        </ProForm.Group>
        <ProForm.Group>
          <ProFormText
            width="lg"
            name="invokeTarget"
            label="调用方法"
          />
        </ProForm.Group>
        <ProForm.Group>
          <ProFormText
            width="lg"
            name="cronExpression"
            label="cron表达式"
          />
        </ProForm.Group>
        <ProForm.Group>
          <ProFormRadio.Group
            width="lg"
            name="misfirePolicy"
            label="执行策略"
            radioType="button"
            initialValue={"3"}
            options={
              [
                {
                  label: '立即执行',
                  value: '1',
                },
                {
                  label: '执行一次',
                  value: '2',
                },
                {
                  label: '放弃执行',
                  value: '3',
                },
              ]
            }
          />
        </ProForm.Group>
        <ProForm.Group>
          <ProFormRadio.Group
            width="md"
            name="concurrent"
            label="是否并发"
            radioType="button"
            initialValue={"1"}
            options={
              [
                {
                  label: '允许',
                  value: '0',
                },
                {
                  label: '禁止',
                  value: '1',
                },
              ]
            }
          />
          <ProFormRadio.Group
            width="md"
            name="status"
            label="状态"
            radioType="radio"
            initialValue={"1"}
            options={
              [
                {
                  label: '正常',
                  value: '0',
                },
                {
                  label: '暂停',
                  value: '1',
                },
              ]
            }
          />
        </ProForm.Group>
      </ModalForm>
      <ModalForm
        title="任务详情"
        visible={visible}
        onVisibleChange={setVisible}
        submitter={{
          searchConfig: {
            resetText: '关闭',
          },
          submitButtonProps: {
            style: {
              display: 'none',
            },
          },
        }}
      >
        <ProDescriptions
          request={() => {
          return  getJob(jobId);
          }}
          column={2}
          columns={[
            {
              title: '任务编号',
              dataIndex: 'jobId',
              ellipsis: true,
            },
            {
              title: '任务分组',
              dataIndex: 'jobGroup',
              render: (text, row) => {
                return <Tag color="blue">{GroupName[row.jobGroup] ? GroupName[row.jobGroup] : row.jobGroup}</Tag>;
              }
            },
            {
              title: '任务名称',
              dataIndex: 'jobName',
              ellipsis: true,
            },
            {
              title: '创建时间',
              dataIndex: 'createTime',
              ellipsis: true,
            },
            {
              title: 'cron执行表达式',
              dataIndex: 'cronExpression',
            },
            {
              title: '下次执行时间',
              dataIndex: 'nextValidTime',
            },
            {
              title: '调用目标方法',
              ellipsis: true,
              dataIndex: 'invokeTarget',
            },
            {
              title: '任务状态',
              dataIndex: 'status',
              valueType: 'select',
              valueEnum: {
                0: {text: '正常', status: 'Processing'},
                1: {text: '暂停', status: 'Error'},
              }
            },
            {
              title: '是否并发',
              dataIndex: 'concurrent',
              valueType: 'select',
              valueEnum: {
                0: {text: '允许', status: 'Processing'},
                1: {text: '禁止', status: 'Error'},
              }
            },
            {
              title: '执行策略',
              dataIndex: 'misfirePolicy',
              valueType: 'select',
              valueEnum: {
                0: {text: '默认', status: 'Processing'},
                1: {text: '立即执行', status: 'Error'},
                2: {text: '执行一次', status: 'Error'},
                3: {text: '放弃执行', status: 'Error'},
              }
            },
          ]}
        >
        </ProDescriptions>
      </ModalForm>
    </PageContainer>
  );
};
