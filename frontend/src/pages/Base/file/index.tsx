import { PageContainer } from '@ant-design/pro-components';
import { Button, List, Space } from 'antd';
import type React from 'react';

interface Props {
  items: string[];
}

const ImgInfo: React.FC<Props> = (props) => {
  const { items } = props;
  return (
    <div>
      {items.length > 0 && (
        <List>
          {items.map((item: string, index: number) => (
            <List.Item key={index}>{item}</List.Item>
          ))}
        </List>
      )}
    </div>
  );
};

const File = () => {
  const props: Props = {
    items: ['1', '2', '3'],
  };

  return (
    <PageContainer>
      <div className="one-block-2">{props.items.length > 0 && <ImgInfo items={props.items} />}</div>
      <div className="one-block-1">
        <span>1. 系统原生对话框</span>
      </div>
      <div className="one-block-2">
        <Space size={'small'}>
          {/*messageShow('ipc')*/}
          <Button type="primary">消息提示(ipc)</Button>
          {/* messageShowConfirm('ipc')*/}
          <Button type="primary">消息提示与确认(ipc)</Button>
        </Space>
      </div>
    </PageContainer>
  );
};

export default File;
