import { UploadOutlined } from '@ant-design/icons';
import { PageContainer } from '@ant-design/pro-components';
import { Button, message, Space, Typography, Upload, UploadProps } from 'antd';
import type React from 'react';

const { Paragraph } = Typography;
const props: UploadProps = {
  name: 'file',
  multiple: true,
  action: process.env.BASE_API + '/api/example/uploadExtension',
  headers: {
    authorization: 'authorization-text',
  },
  onChange(info) {
    if (info.file.status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};
const Extension: React.FC<unknown> = () => {
  return (
    <PageContainer>
      <div className="one-block-1">
        <span>1. 上传扩展程序（crx文件格式）</span>
      </div>
      <div>
        <Upload {...props}>
          <Button icon={<UploadOutlined />}>Click to Upload</Button>
        </Upload>
      </div>
      <div className="one-block-1">2. chrome扩展商店（crx下载）</div>
      <div className="one-block-2">
        <Space align={'start'}>
          极简插件 :<Paragraph copyable>https://chrome.zzzmh.cn/</Paragraph>
        </Space>
      </div>
    </PageContainer>
  );
};

export default Extension;
