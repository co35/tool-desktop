import { ipcApiRoute } from '@/endpoint/main';
import IpcRender from '@/utils/ipcRender';
import { PageContainer } from '@ant-design/pro-components';
import { Button, Col, Input, message, Row } from 'antd';
import React, { useEffect, useState } from 'react';
import styles from './index.less';
const DataBase: React.FC<unknown> = () => {
  const [dbLocal, setDbLocal] = useState([]);
  const [userList, setUserList] = useState([]);
  const [user, setUser] = useState({ name: '', age: '' });
  const [searchAge, setSearchAge] = useState('');
  const [updateName, setUpdateName] = useState('');
  const [updateAge, setUpdateAge] = useState('');
  const [delName, setDelName] = useState('');
  const dbOperation = (ac: string) => {
    const params = {
      action: ac,
      info: {
        name: user.name,
        age: parseInt(user.age),
      },
      search_age: parseInt(searchAge),
      update_name: updateName,
      update_age: parseInt(updateAge),
      delete_name: delName,
    };
    if (ac == 'add' && user.name.length == 0) {
      message.error('请填写数据');
      return;
    }
    IpcRender.invoke(ipcApiRoute.dbOperation, params).then((res) => {
      if (ac == 'get') {
        if (res.result.length == 0) {
          message.error('没有数据');
          return;
        }
        setUserList(res.result);
      }
      if (res.all_list.length == 0) {
        // @ts-ignore
        setDbLocal(['空']);
        return;
      }
      setDbLocal(res.all_list);
      message.success(`success`);
    });
  };

  const getUser = () => {
    IpcRender.invoke(ipcApiRoute.dbOperation, {
      action: 'all',
    })
      // @ts-ignore
      .then((res) => {
        console.log(res);
        if (res.all_list.length == 0) {
          return false;
        }
        setDbLocal(res.all_list);
      });
  };
  useEffect(getUser, []);
  return (
    <PageContainer>
      <div className={styles.appBaseDb}>
        <div>
          <span>1,本地数据库</span>
        </div>
        <div>
          <Row>
            <Col span={8}>LowDB本地JSON数据库</Col>
            <Col span={8}>可以使用loadsh语法</Col>
            <Col span={8}></Col>
          </Row>
        </div>
        <div>
          <span>2,测试数据</span>
        </div>
        <div>
          <Row>
            <Col span={24}>{JSON.stringify(dbLocal)}</Col>
          </Row>
        </div>
        <div>
          <span>3,添加数据</span>
        </div>
        <div>
          <Row>
            <Col span={6}>
              <Input
                placeholder={'姓名'}
                value={user.name}
                onChange={(e) => {
                  setUser({ ...user, name: e.target.value });
                }}
              />
            </Col>
            <Col span={3}></Col>
            <Col span={6}>
              <Input
                placeholder={'年龄'}
                value={user.age}
                onChange={(e) => {
                  setUser({ ...user, age: e.target.value });
                }}
              />
            </Col>
            <Col span={3}></Col>
            <Col span={6}>
              <Button
                type="primary"
                onClick={() => {
                  dbOperation('add');
                }}
              >
                添加
              </Button>
            </Col>
          </Row>
        </div>
        <div className="one-block-1">
          <span>4. 获取数据</span>
        </div>
        <div>
          <Row>
            <Col span={6}>
              <Input
                placeholder={'年龄'}
                value={searchAge}
                onChange={(e) => {
                  setSearchAge(e.target.value);
                }}
              />
            </Col>
            <Col span={3}></Col>
            <Col span={6}></Col>
            <Col span={3}></Col>
            <Col span={6}>
              <Button
                type="primary"
                onClick={() => {
                  dbOperation('get');
                }}
              >
                查找
              </Button>
            </Col>
          </Row>
          <Row>
            <Col span={24}>{JSON.stringify(userList)}</Col>
          </Row>
        </div>
        <div className="one-block-1">
          <span>5. 修改数据</span>
        </div>
        <div>
          <Row>
            <Col span={6}>
              <Input
                placeholder={'姓名'}
                value={updateName}
                onChange={(e) => {
                  setUpdateName(e.target.value);
                }}
              />
            </Col>
            <Col span={3}></Col>
            <Col span={6}>
              <Input
                placeholder={'年龄'}
                value={updateAge}
                onChange={(e) => {
                  setUpdateAge(e.target.value);
                }}
              />
            </Col>
            <Col span={3}></Col>
            <Col span={6}>
              <Button type={'primary'} onClick={() => dbOperation('update')}>
                更新
              </Button>
            </Col>
          </Row>
        </div>
        <div className="one-block-1">
          <span>6. 删除数据</span>
        </div>
        <div>
          <Row>
            <Col span={6}>
              <Input
                placeholder={'姓名'}
                value={delName}
                onChange={(e) => setDelName(e.target.value)}
              />
            </Col>
            <Col span={3}></Col>
            <Col span={6}></Col>
            <Col span={3}></Col>
            <Col span={6}>
              <Button type="primary" onClick={() => dbOperation('del')}>
                删除
              </Button>
            </Col>
          </Row>
        </div>
      </div>
    </PageContainer>
  );
};

export default DataBase;
