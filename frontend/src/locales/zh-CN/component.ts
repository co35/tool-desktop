export default {
  'component.tagSelect.expand': '展开',
  'component.tagSelect.collapse': '收起',
  'component.tagSelect.all': '全部',
  'component.switchTheme.light': '浅色',
  'component.switchTheme.dark': '深色',
};
