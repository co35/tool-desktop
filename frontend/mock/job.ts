import { Request, Response } from 'express';
import moment from 'moment';

const getJobs  = (pageNum: number, pageSize: number) => {
  const rows:API.Job[] = [];
  for (let i = 0; i < pageSize; i += 1) {
    const index = (pageNum - 1) * 10 + i;
    rows.push({
      updateBy: 'job',
      updateTime: moment().format('YYYY-MM-DD'),
      createBy: "job",
      createTime:moment().format('YYYY-MM-DD'),
      params: {},
      jobId: index,
      jobName: "test",
      jobGroup: "DEFAULT",
      invokeTarget: "sfaPageTask.run('"+Math.floor(Math.random() * 1000)+"','"+Math.floor(Math.random() * 1000)+"',5000L)",
      cronExpression: "0 0 20 * * ?",
      misfirePolicy: "1",
      concurrent: "0",
      status: i % 6 === 0?"1":"0",
      nextValidTime: "2022-08-05 12:00:00"

    })
  }
  return rows;
}

const listJobs = (req: Request, res: Response, u: string) => {
  console.log('mock job')
  const { pageNum =1, pageSize =10} = req.query;
  let jobs =getJobs(1,100).slice(
    ((pageNum as number) - 1) * (pageSize as number),
    (pageNum as number) * (pageSize as number),
  );
  let rows=[...jobs]
  let resData:API.TableDataInfo<API.Job>={
    total: 100,
    rows: rows,
    code: 200,
    msg: '查询成功'
  };

  res.send(resData);
}
const getJob = (req: Request, res: Response, u: string) => {
  const {jobId =0} = req.query;
  let jobs =getJobs(1,100);
  let job = jobs.at(jobId as number);
  let resData:API.Result<any>={
    code: 200,
    msg: '查询成功',
    data: job
  };
  res.send(resData);
}
export default {
  'GET /api/job/list': listJobs,
  'GET /api/job/':getJob,
  'DELETE /api/job': {
    code: 200,
    msg: '删除成功',
    data: null
  },
  'PUT /api/job': {
      code: 200,
      msg: '修改成功',
      data: 0
    },
  'POST /api/job': {
    code: 200,
    msg: '新增成功',
    data: 1
  }


}
