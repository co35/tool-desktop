﻿export default [
  {
    path: '/user',
    layout: false,
    routes: [
      {
        name: 'login',
        path: '/user/login',
        component: './User/Login',
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/welcome',
    name: 'welcome',
    icon: 'smile',
    component: './Welcome',
  },
  {
    name: "base",
    path: "/base",
    icon:"ToolOutlined",
    routes: [
      {
        name: "localdb",
        path: "/base/db",
        component: "./Base/db",
      },
      {
        name: "extension",
        path: "/base/extension",
        component: "./Base/extension",
      },
      {
        name: "file",
        path: "/base/file",
        component: "./Base/file",
      }
    ]
  },
  // {
  //   path: '/admin',
  //   name: 'admin',
  //   icon: 'crown',
  //   access: 'canAdmin',
  //   routes: [
  //     {
  //       path: '/admin/sub-page',
  //       name: 'sub-page',
  //       icon: 'smile',
  //       component: './Welcome',
  //     },
  //     {
  //       component: './404',
  //     },
  //   ],
  // },
  {
    name: 'list.table-list',
    icon: 'table',
    path: '/list',
    component: './TableList',
  },
  {
    name: 'job',
    icon: 'CodeSandboxOutlined',
    path: '/job',
    routes: [
      {
        name: "job-list",
        path: "/job/list",
        component: './Job',
      },
      {
        hideInMenu: true,
        name: "job-log",
        path: "/job/log",
        component: './Job/jobLog',
      },
    ]

  },
  {
    path: '/',
    redirect: '/welcome',
  },
  {
    component: './404',
  },
];
